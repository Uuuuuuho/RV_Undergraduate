# # .include "user_define.h"
.globl _start
# .local _start
# .align 13
.section .text
_start:
     addi   x2,zero,0x20
     add    x3,x2,x2
     add    x4,x3,x3
     li     x5, 0x00000100
     li     x6, 0x00001100
     li     x7, 0x00002100
     sw     x2, 0x0(x5)
     sw     x2, 0x4(x5)
     sw     x2, 0x8(x5)
     sw     x2, 0xc(x5)
     sw     x3, 0x0(x5)
     sw     x4, 0x0(x6)
     sw     x4, 0x0(x7)
     lw     x2, 0x0(x5)
