gui_state_default_create -off -ini

# Globals
gui_set_state_value -category Globals -key recent_databases -value {{gui_open_db -file /home/uho/workspace/RISCV_RTL_SIM/sim/dump/Dev_top_test.vpd -design V1} {gui_open_db -file /home/uho/workspace/RISCV_RTL_SIM/sim/dump/Dev_top_test.vpd -design V2}}
gui_set_state_value -category Globals -key recent_sessions -value {{gui_load_session -ignore_errors -file /home/uho/workspace/RISCV_RTL_SIM/sim/DVEfiles//crash_02.19-14.01/dve_session02.19-14.01.tcl}}

# Layout
gui_set_state_value -category Layout -key child_console_size_x -value 1854
gui_set_state_value -category Layout -key child_console_size_y -value 178
gui_set_state_value -category Layout -key child_data_coltype -value 76
gui_set_state_value -category Layout -key child_data_colvalue -value 109
gui_set_state_value -category Layout -key child_data_colvariable -value 229
gui_set_state_value -category Layout -key child_data_size_x -value 423
gui_set_state_value -category Layout -key child_data_size_y -value 802
gui_set_state_value -category Layout -key child_hier_col3 -value {-1}
gui_set_state_value -category Layout -key child_hier_colhier -value 232
gui_set_state_value -category Layout -key child_hier_colpd -value 0
gui_set_state_value -category Layout -key child_hier_coltype -value 103
gui_set_state_value -category Layout -key child_hier_size_x -value 345
gui_set_state_value -category Layout -key child_hier_size_y -value 802
gui_set_state_value -category Layout -key child_source_docknewline -value false
gui_set_state_value -category Layout -key child_source_pos_x -value {-2}
gui_set_state_value -category Layout -key child_source_pos_y -value {-15}
gui_set_state_value -category Layout -key child_source_size_x -value 1089
gui_set_state_value -category Layout -key child_source_size_y -value 797
gui_set_state_value -category Layout -key child_wave_colname -value 220
gui_set_state_value -category Layout -key child_wave_colvalue -value 88
gui_set_state_value -category Layout -key child_wave_left -value 312
gui_set_state_value -category Layout -key child_wave_right -value 762
gui_set_state_value -category Layout -key main_pos_x -value 64
gui_set_state_value -category Layout -key main_pos_y -value 546
gui_set_state_value -category Layout -key main_size_x -value 1919
gui_set_state_value -category Layout -key main_size_y -value 1602
gui_set_state_value -category Layout -key stand_wave_child_docknewline -value false
gui_set_state_value -category Layout -key stand_wave_child_pos_x -value {-2}
gui_set_state_value -category Layout -key stand_wave_child_pos_y -value {-15}
gui_set_state_value -category Layout -key stand_wave_child_size_x -value 1084
gui_set_state_value -category Layout -key stand_wave_child_size_y -value 1752
gui_set_state_value -category Layout -key stand_wave_top_pos_x -value 1920
gui_set_state_value -category Layout -key stand_wave_top_pos_y -value 63
gui_set_state_value -category Layout -key stand_wave_top_size_x -value 2999
gui_set_state_value -category Layout -key stand_wave_top_size_y -value 1919

# list_value_column

# Sim

# Assertion

# Stream

# Data

# TBGUI

# Driver

# Class

# Member

# ObjectBrowser

# UVM

# Local

# Backtrace

# FastSearch

# Exclusion

# SaveSession

# FindDialog
gui_create_state_key -category FindDialog -key m_pMatchCase -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pMatchWord -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pUseCombo -value_type string -value {}
gui_create_state_key -category FindDialog -key m_pWrapAround -value_type bool -value true

# Widget_History
gui_create_state_key -category Widget_History -key TopLevel.2|EkTopVbox|wndWorkspace|qt_workspacechild2|Wave.1|left|filterVBox|controlHBox|unnamed -value_type string -value *pc


gui_state_default_create -off
