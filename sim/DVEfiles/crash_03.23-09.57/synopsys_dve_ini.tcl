gui_state_default_create -off -ini

# Globals
gui_set_state_value -category Globals -key recent_databases -value {{gui_open_db -file /home/uho/workspace/RISCV_RTL_SIM/sim/dump/Dev_top_test.vpd -design V1} {gui_open_db -file /home/uho/workspace/RISCV_RTL_SIM/sim/dump/Dev_top_test.vpd -design V2}}
gui_set_state_value -category Globals -key recent_sessions -value {{gui_load_session -ignore_errors -file /home/uho/workspace/RISCV_RTL_SIM/sim/DVEfiles//crash_03.23-09.57/dve_session03.23-09.57.tcl} {gui_load_session -ignore_errors -file /home/uho/workspace/RISCV_RTL_SIM/sim/DVEfiles//crash_03.23-09.26/dve_session03.23-09.26.tcl} {gui_load_session -ignore_errors -file /home/uho/workspace/RISCV_RTL_SIM/sim/DVEfiles//crash_03.22-17.25/dve_session03.22-17.25.tcl} {gui_load_session -ignore_errors -file /home/uho/workspace/RISCV_RTL_SIM/sim/DVEfiles//crash_03.12-23.33/dve_session03.12-23.33.tcl} {gui_load_session -ignore_errors -file /home/uho/workspace/RISCV_RTL_SIM/sim/DVEfiles//crash_03.12-16.13/dve_session03.12-16.13.tcl}}

# Layout
gui_set_state_value -category Layout -key child_console_size_x -value 1855
gui_set_state_value -category Layout -key child_console_size_y -value 169
gui_set_state_value -category Layout -key child_data_coltype -value 104
gui_set_state_value -category Layout -key child_data_colvalue -value 137
gui_set_state_value -category Layout -key child_data_colvariable -value 257
gui_set_state_value -category Layout -key child_data_size_x -value 503
gui_set_state_value -category Layout -key child_data_size_y -value 810
gui_set_state_value -category Layout -key child_hier_col3 -value {-1}
gui_set_state_value -category Layout -key child_hier_colhier -value 232
gui_set_state_value -category Layout -key child_hier_colpd -value 0
gui_set_state_value -category Layout -key child_hier_coltype -value 88
gui_set_state_value -category Layout -key child_hier_size_x -value 322
gui_set_state_value -category Layout -key child_hier_size_y -value 810
gui_set_state_value -category Layout -key child_list_down -value 203
gui_set_state_value -category Layout -key child_list_right -value 444
gui_set_state_value -category Layout -key child_schematic_docknewline -value false
gui_set_state_value -category Layout -key child_schematic_pos_x -value {-2}
gui_set_state_value -category Layout -key child_schematic_pos_y -value {-15}
gui_set_state_value -category Layout -key child_schematic_size_x -value 250
gui_set_state_value -category Layout -key child_schematic_size_y -value 1579
gui_set_state_value -category Layout -key child_source_docknewline -value false
gui_set_state_value -category Layout -key child_source_pos_x -value {-2}
gui_set_state_value -category Layout -key child_source_pos_y -value {-15}
gui_set_state_value -category Layout -key child_source_size_x -value 1033
gui_set_state_value -category Layout -key child_source_size_y -value 805
gui_set_state_value -category Layout -key child_watch_size_x -value 1854
gui_set_state_value -category Layout -key child_watch_size_y -value 340
gui_set_state_value -category Layout -key child_wave_colname -value 267
gui_set_state_value -category Layout -key child_wave_colvalue -value 267
gui_set_state_value -category Layout -key child_wave_docknewline -value false
gui_set_state_value -category Layout -key child_wave_left -value 538
gui_set_state_value -category Layout -key child_wave_pos_x -value {-2}
gui_set_state_value -category Layout -key child_wave_pos_y -value {-15}
gui_set_state_value -category Layout -key child_wave_right -value 1313
gui_set_state_value -category Layout -key child_wave_size_x -value 1859
gui_set_state_value -category Layout -key child_wave_size_y -value 635
gui_set_state_value -category Layout -key main_pos_x -value 70
gui_set_state_value -category Layout -key main_pos_y -value 577
gui_set_state_value -category Layout -key main_size_x -value 1925
gui_set_state_value -category Layout -key main_size_y -value 1631
gui_set_state_value -category Layout -key stand_list_child_docknewline -value false
gui_set_state_value -category Layout -key stand_list_child_pos_x -value {-2}
gui_set_state_value -category Layout -key stand_list_child_pos_y -value {-15}
gui_set_state_value -category Layout -key stand_list_child_size_x -value 604
gui_set_state_value -category Layout -key stand_list_child_size_y -value 348
gui_set_state_value -category Layout -key stand_list_top_pos_x -value 278
gui_set_state_value -category Layout -key stand_list_top_pos_y -value 762
gui_set_state_value -category Layout -key stand_list_top_size_x -value 877
gui_set_state_value -category Layout -key stand_list_top_size_y -value 1261
gui_set_state_value -category Layout -key stand_source_child_docknewline -value false
gui_set_state_value -category Layout -key stand_source_child_pos_x -value {-2}
gui_set_state_value -category Layout -key stand_source_child_pos_y -value {-15}
gui_set_state_value -category Layout -key stand_source_child_size_x -value 504
gui_set_state_value -category Layout -key stand_source_child_size_y -value 248
gui_set_state_value -category Layout -key stand_source_top_pos_x -value 96
gui_set_state_value -category Layout -key stand_source_top_pos_y -value 580
gui_set_state_value -category Layout -key stand_source_top_size_x -value 595
gui_set_state_value -category Layout -key stand_source_top_size_y -value 979
gui_set_state_value -category Layout -key stand_wave_child_docknewline -value false
gui_set_state_value -category Layout -key stand_wave_child_pos_x -value {-2}
gui_set_state_value -category Layout -key stand_wave_child_pos_y -value {-15}
gui_set_state_value -category Layout -key stand_wave_child_size_x -value 1861
gui_set_state_value -category Layout -key stand_wave_child_size_y -value 975
gui_set_state_value -category Layout -key stand_wave_top_pos_x -value 62
gui_set_state_value -category Layout -key stand_wave_top_pos_y -value 546
gui_set_state_value -category Layout -key stand_wave_top_size_x -value 1918
gui_set_state_value -category Layout -key stand_wave_top_size_y -value 1601

# list_value_column

# Sim

# Assertion

# Stream

# Data

# TBGUI

# Driver

# Class

# Member

# ObjectBrowser

# UVM

# Local

# Backtrace

# FastSearch

# Exclusion

# SaveSession

# FindDialog
gui_create_state_key -category FindDialog -key m_pMatchCase -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pMatchWord -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pUseCombo -value_type string -value {}
gui_create_state_key -category FindDialog -key m_pWrapAround -value_type bool -value true

# Widget_History
gui_create_state_key -category Widget_History -key Find|m_pFindFrame|m_pFindCombo -value_type string -value {tb_inst_data_i tb_inst_addr_i pc mem_pc_ mem_pc_i}
gui_create_state_key -category Widget_History -key TopLevel.1|qt_left_dock|DockWnd2|DLPane.1|pages|Data.1|hbox|textfilter -value_type string -value {*pc* *mem_data_rd_o* *mem_addr_i* *muxed_wr_w*}
gui_create_state_key -category Widget_History -key TopLevel.1|qt_top_dock|&Edit|FindCombo -value_type string -value {tb_inst_data_i tb_inst_addr_i pc mem_pc_ mem_pc_i}
gui_create_state_key -category Widget_History -key TopLevel.2|EkTopVbox|wndWorkspace|qt_workspacechild2|Wave.1|left|filterVBox|controlHBox|unnamed -value_type string -value {*mem_addr_m_q *pc}
gui_create_state_key -category Widget_History -key TopLevel.2|qt_top_dock|&Edit|FindCombo -value_type string -value {tb_inst_data_i tb_inst_addr_i pc mem_pc_ mem_pc_i}


gui_state_default_create -off
