//========================================
//  COMMIT (Compiler & Micro-architecture)
//========================================
//Seok Young Kim.

이번에 진행되는 프로젝트는 크게 2가지를 진행하게 됩니다.
 1. RISC-V의 register의 역할을 이해하고, function call시에 각 register들의 변화를 관찰한다. 
 2. Hardware performance를 측정할 수 있는 counter를 내부에 설계하고, 이를 검증한다.
    * 2번은 project4에 확장되어 사용할 예정입니다.

폴더 트리에 대한 설명은 아래와 같습니다. 
//Problem1 
  - HW_RTL_src     : RISC-V core의 source code
  - problem1_tb    : 문제1의 testbench 
  - SW_code        : 문제1의 C code와 이를 컴파일하기 위한 source code, makefile 
//Problem2
  - HW_RTL_src     : RISC-V core의 source code (문제1과 동일)
  - problem2_tb    : 문제2의 testbench 

각각의 문제에서 학생들이 공부해야할 코드 목록은 다음과 같습니다. 

//Problem1 
  - SW_code/problem1.c
  - SW_code/Makefile
  - SW_code/out.dump
  - HW_RTL_src/core/riscv_regfile.v
    * Call sequence에 따른 stack의 layout그림은 강의자료를 참고하시길 바랍니다. 
//Problem2 
  - HW_RTL_src/core/risc_decode.v
  - HW_RTL_src/core/riscv_fetch.v
  - HW_RTL_src/core/risc_pipe_ctrl.v
  - HW_RTL_src/core/riscv_HPC.v
    * 문제에서 알 수 있듯이, riscv_HPC는 조교가 in,out과 counter register를 미리 작성하여 놓은 상태입니다. In, out, counter를 수정하지 않은 채로 진행하시길 바랍니다.




  