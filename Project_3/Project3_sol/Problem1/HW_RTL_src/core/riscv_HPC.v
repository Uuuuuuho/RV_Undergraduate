module riscv_HPC (
    
    clk, 
    rst_i,

    //Request information from pipline controller
    req_pc,
    req_next_pc,
    req_inst_opcode,
    req_inst_valid,
    
    
    //Not used in Project 3, Interface for future use.
      // Committed & retired instruction 
      req_inst_commit,
      req_inst_retired,
      //ALU bound instruction info.
      req_inst_ALU,
      req_inst_div,
      req_inst_mul,
      stall_by_ALU,
      //Memory bound instruction info. 
      req_inst_load,
      req_inst_store,
      stall_by_MEM,
      //Control flow bound instruction info. 
      req_inst_branch,
      req_branch_taken,
      req_branch_inst_pc,
      req_branch_target

);


    input        clk;
    input        rst_i;

    input [31:0] req_pc;
    input [31:0] req_next_pc;
    input [31:0] req_inst_opcode;
    input        req_inst_valid;

    input        req_inst_commit;
    input        req_inst_retired;

    input        req_inst_ALU;
    input        req_inst_div;
    input        req_inst_mul;
    input        stall_by_ALU;

    input        req_inst_load;
    input        req_inst_store;
    input        stall_by_MEM;

    input        req_inst_branch;
    input        req_branch_taken;
    input [31:0] req_branch_inst_pc;
    input [31:0] req_branch_target;
    
    
/*
Project 3
*/

    reg [31:0]  HPC_req_Rtype;
    reg [31:0]  HPC_req_Itype;
    reg [31:0]  HPC_req_Stype;
    reg [31:0]  HPC_req_Btype;
    reg [31:0]  HPC_req_Utype;
    reg [31:0]  HPC_req_Jtype;

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Rtype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b011_0011)  HPC_req_Rtype <= HPC_req_Rtype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Itype <='b0;
    else if(req_inst_valid 
           && ((req_inst_opcode[6:0] == 7'b000_0011)  //Load
              ||(req_inst_opcode[6:0] == 7'b001_0011) //Arithmetic -I
              ||(req_inst_opcode[6:0] == 7'b011_0011) //Arithmetic 
              ||(req_inst_opcode[6:0] == 7'b110_0111) //Jalr 
              )
            )                                       HPC_req_Itype <= HPC_req_Itype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Stype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b010_0011)  HPC_req_Stype <= HPC_req_Stype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Btype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b110_0011)  HPC_req_Btype <= HPC_req_Btype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Utype <='b0;
    else if(req_inst_valid 
           && ((req_inst_opcode[6:0] == 7'b011_0111) 
              ||(req_inst_opcode[6:0] == 7'b001_0111)
              )
            )                                       HPC_req_Utype <= HPC_req_Utype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Jtype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b110_1111)  HPC_req_Jtype <= HPC_req_Jtype +1'b1;
end

/*
Project 4
*/
    reg [31:0]   HPC_req_commit;
    reg [31:0]   HPC_req_retired;

    reg [31:0]   HPC_req_ALU;
    reg [31:0]   HPC_req_ALU_stall_cycle;
    reg [31:0]   HPC_req_MEM;
    reg [31:0]   HPC_req_MEM_stall_cycle;


// Total commit & retired instruction #
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)            HPC_req_commit  <='b0;
    else if(req_inst_commit)  HPC_req_commit  <= HPC_req_commit + 1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if     (rst_i)            HPC_req_retired  <='b0;
    else if(req_inst_retired) HPC_req_retired  <= HPC_req_retired + 1'b1;
end


//ALU bound instruction & stall cycle 
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_ALU  <='b0;
    else if(req_inst_div 
            ||req_inst_mul
            ||req_inst_ALU
    )        
                                         HPC_req_ALU  <= HPC_req_ALU + 1'b1;
end
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_ALU_stall_cycle  <='b0;
    else if(stall_by_ALU)                HPC_req_ALU_stall_cycle  <= HPC_req_ALU_stall_cycle + 1'b1;  
end


//Memory bound instruction & stall cycle
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                         HPC_req_MEM  <='b0;
    else if(req_inst_load
            ||req_inst_store
            )                              HPC_req_MEM  <= HPC_req_MEM + 1'b1;
end
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_MEM_stall_cycle  <='b0;
    else if(stall_by_MEM)                HPC_req_MEM_stall_cycle  <= HPC_req_MEM_stall_cycle + 1'b1;  
end
// Branch instruction info 
/* Branch Target Buffer 
   |Branch taken or not taken| 1-bit      //1 = taken, 0 = not taken
   |Branch instruction PC    | 32-bit     
   |Branch target            | 32-bit  
        ==> |-----| 65-bit

   BTB: Entry size 16
   Addr     Data
   0      |-----| 65-bit
   1      |-----| 65-bit
      ..
   15     |-----| 65-bit
*/

    reg  [31:0] HPC_req_BRANCH;            //Counter for #of branch inst.
    reg  [31:0] HPC_req_BRANCH_taken;      //Counter for #of branch inst. taken
    reg  [64:0] HPC_branch_info_data[0:15];//Register for branch inst. metadata
    reg  [3:0]  HPC_branch_info_addr;      //Address for branch inst. metadata
    wire [3:0]  HPC_branch_info_addr_w;
    wire        HPC_branch_info_full;

    assign HPC_branch_info_addr_w = HPC_branch_info_addr;
    assign HPC_branch_info_full   = (HPC_branch_info_addr == 4'b1111);

always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_BRANCH  <='b0;
    else if(req_inst_branch)             HPC_req_BRANCH  <= HPC_req_BRANCH + 1'b1;  
end

always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_BRANCH_taken  <='b0;
    else if(req_branch_taken)            HPC_req_BRANCH_taken  <= HPC_req_BRANCH_taken + 1'b1;  
end

always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_branch_info_addr  <= 'b0;
    else if(!HPC_branch_info_full 
             &&req_inst_branch)          HPC_branch_info_addr  <=  HPC_branch_info_addr + 1'b1;  
    else if(HPC_branch_info_full)        HPC_branch_info_addr  <= 4'b1111;
end

always @(posedge rst_i or posedge clk) begin
    if(!HPC_branch_info_full 
        &&req_inst_branch)begin
        HPC_branch_info_data[HPC_branch_info_addr_w] = {req_branch_taken, req_branch_inst_pc, req_branch_target};
    end
end


    


endmodule