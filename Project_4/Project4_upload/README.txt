//========================================
//  COMMIT (Compiler & Micro-architecture)
//========================================
//Seok Young Kim.

이번에 진행되는 프로젝트는 크게 2가지를 진행하게 됩니다.
 1. Application code (DFS on graph)를 통해 data & control hazard와 이에 따른 stall을 HPC로 측정하도록 project3을 확장한다. 
 2. Branch predictor를 설계하고, 이를 검증한다.  
    
폴더 트리에 대한 설명은 아래와 같습니다. 
//Problem1 
  - HW_RTL_source      
	-RISCV          : RISC-V core의 source code
            -testbench     : 문제1과 2의 testbench //동일한 C code를 실행하므로, testbench가 동일합니다. 
  - SW_C_code            : 공통 문제의 C code와 이를 컴파일하기 위한 source code, makefile 


각각의 문제에서 학생들이 공부해야할 코드 목록은 다음과 같습니다. 
 - 이번 프로젝트는 SW code의 동작보다는 RISC-V core 내부의 pipeline이 어떻게 동작하는지 이해하는 것이 가장 중요합니다. 
   따라서, C code를 공부하기 보다는 RISC-V core 내부에 있는 아래의 code를 중점적으로 학습하면 됩니다. 

//Problem1 
  - HW_RTL_source/top_src/core/riscv_issue.v
  - HW_RTL_source/top_src/core/riscv_pipe_ctrl.v
  - HW_RTL_source/top_src/core/riscv_HPC.v

//Problem2 
  - SW_C_code/problem1.c
  - SW_C_code/out.dump
  - HW_RTL_source/top_src/core/riscv_issue.v
  - HW_RTL_source/top_src/core/riscv_pipe_ctrl.v
  - HW_RTL_source/top_src/core/riscv_HPC.v
    * 문제에서 알 수 있듯이, riscv_HPC는 조교가 in,out과 counter register를 미리 작성하여 놓은 상태입니다.
      Branch predictor의 동작과 알고리즘을 정확히 이해하고, 제가 선언해놓은 신호들을 hint를 사용하여 작성해보길 바랍니다.





  