module riscv_HPC (
    
    clk, 
    rst_i,

    //Request information from pipline controller
    req_pc,
    req_next_pc,
    req_inst_opcode,
    req_inst_valid,
    
    
    //Not used in Project 3, Interface for future use.
      // Committed & retired instruction 
      req_inst_commit,
      req_inst_retired,
      //ALU bound instruction info.
      req_inst_ALU,
      req_inst_div,
      req_inst_mul,
      stall_by_ALU,
      //Memory bound instruction info. 
      req_inst_load,
      req_inst_store,
      stall_by_MEM,
      //Control flow bound instruction info. 
      req_inst_branch,
      req_branch_taken,
      req_branch_inst_pc,
      req_branch_target

);


    input        clk;
    input        rst_i;

    input [31:0] req_pc;
    input [31:0] req_next_pc;
    input [31:0] req_inst_opcode;
    input        req_inst_valid;

    input        req_inst_commit;
    input        req_inst_retired;

    input        req_inst_ALU;
    input        req_inst_div;
    input        req_inst_mul;
    input        stall_by_ALU;

    input        req_inst_load;
    input        req_inst_store;
    input        stall_by_MEM;

    input        req_inst_branch;
    input        req_branch_taken;
    input [31:0] req_branch_inst_pc;
    input [31:0] req_branch_target;
    
    
/*
Project 3
*/

    reg [31:0]  HPC_req_Rtype;
    reg [31:0]  HPC_req_Itype;
    reg [31:0]  HPC_req_Stype;
    reg [31:0]  HPC_req_Btype;
    reg [31:0]  HPC_req_Utype;
    reg [31:0]  HPC_req_Jtype;

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Rtype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b011_0011)  HPC_req_Rtype <= HPC_req_Rtype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Itype <='b0;
    else if(req_inst_valid 
           && ((req_inst_opcode[6:0] == 7'b000_0011)  //Load
              ||(req_inst_opcode[6:0] == 7'b001_0011) //Arithmetic -I
              ||(req_inst_opcode[6:0] == 7'b011_0011) //Arithmetic 
              ||(req_inst_opcode[6:0] == 7'b110_0111) //Jalr 
              )
            )                                       HPC_req_Itype <= HPC_req_Itype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Stype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b010_0011)  HPC_req_Stype <= HPC_req_Stype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Btype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b110_0011)  HPC_req_Btype <= HPC_req_Btype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Utype <='b0;
    else if(req_inst_valid 
           && ((req_inst_opcode[6:0] == 7'b011_0111) 
              ||(req_inst_opcode[6:0] == 7'b001_0111)
              )
            )                                       HPC_req_Utype <= HPC_req_Utype +1'b1;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                                       HPC_req_Jtype <='b0;
    else if(req_inst_valid 
           && req_inst_opcode[6:0] == 7'b110_1111)  HPC_req_Jtype <= HPC_req_Jtype +1'b1;
end


/*
Project 4
*/
    reg [31:0]   HPC_req_retired;
    reg [63:0]   HPC_exe_cycle;

    reg [31:0]   HPC_req_ALU;
    reg [31:0]   HPC_req_ALU_stall_cycle;
    reg [31:0]   HPC_req_MEM;
    reg [31:0]   HPC_req_MEM_stall_cycle;
    reg [31:0]   HPC_req_MEM_cause_stall;
    
    reg [31:0]   HPC_req_BRANCH_taken;
    reg [31:0]   HPC_req_jalr;
    
    reg          req_inst_load_r;
    reg          req_inst_store_r;
    wire         req_conditional_branch;
    reg          req_conditional_branch_r;
    reg          req_branch_taken_r;

assign req_conditional_branch = (req_inst_branch && req_inst_opcode[6:0] == 7'b110_0011);
always @(posedge rst_i or posedge clk) begin
    if(rst_i)                  req_conditional_branch_r <= 1'b0;
    else                       req_conditional_branch_r <= req_conditional_branch;
end

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                  req_branch_taken_r <= 1'b0;
    else                       req_branch_taken_r <= req_branch_taken;
end

always @(posedge rst_i or posedge clk) begin
    if     (rst_i)            HPC_req_retired  <='b0;
    else if(req_inst_retired) HPC_req_retired  <= HPC_req_retired + 1'b1;
end


always @(posedge rst_i or posedge clk) begin
    if     (rst_i)            HPC_exe_cycle  <='b0;
    else                      HPC_exe_cycle  <= HPC_exe_cycle + 1'b1;
end


//ALU bound instruction & stall cycle 
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_ALU  <='b0;
    else if(req_inst_div 
            //||req_inst_mul
            //||req_inst_ALU
    )        
                                         HPC_req_ALU  <= HPC_req_ALU + 1'b1;
end
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_ALU_stall_cycle  <='b0;
    else if(stall_by_ALU)                HPC_req_ALU_stall_cycle  <= HPC_req_ALU_stall_cycle + 1'b1;  
end


//Memory bound instruction & stall cycle
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                         HPC_req_MEM  <='b0;
    else if(req_inst_load
            ||req_inst_store
            )                              HPC_req_MEM  <= HPC_req_MEM + 1'b1;
end
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_MEM_stall_cycle  <='b0;
    else if(stall_by_MEM)                HPC_req_MEM_stall_cycle  <= HPC_req_MEM_stall_cycle + 1'b1;  
end

always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                       HPC_req_BRANCH_taken  <='b0;
    else if(req_branch_taken)            HPC_req_BRANCH_taken  <= HPC_req_BRANCH_taken + 1'b1;  
end
always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                                 HPC_req_jalr  <='b0;
    else if((req_inst_opcode[6:0] == 7'b110_0111)) HPC_req_jalr  <= HPC_req_jalr + 1'b1;  
end




wire  stall_sig_rising_edge;
assign stall_sig_rising_edge = stall_by_MEM;
reg stall_sig_rising_edge_r;
 always @(posedge rst_i or posedge clk) begin
     if     (rst_i)                          stall_sig_rising_edge_r  <=1'b0;
     else                                    stall_sig_rising_edge_r  <=stall_sig_rising_edge;  
 end
wire stall_MEM_cause_sig;
assign stall_MEM_cause_sig = (stall_sig_rising_edge==1'b1)&&(stall_sig_rising_edge_r==1'b0);


always @(posedge rst_i or posedge clk) begin
    if     (rst_i)                          HPC_req_MEM_cause_stall  <='b0;
    else if(stall_MEM_cause_sig)            HPC_req_MEM_cause_stall  <= HPC_req_MEM_cause_stall + 1'b1;  
end


//===============================================
//  problem 2 related
//===============================================

localparam FETCH_PC_BHT_INDEX_L     = 2;  
localparam FETCH_PC_BHT_INDEX_H     = 5;
`define FETCH_PC_BHT_INDEX  FETCH_PC_BHT_INDEX_H:FETCH_PC_BHT_INDEX_L

/* Branch History Table 

   BHT: Entry size 16
   Addr     Data
   0      |-----| 2-bit
   1      |-----| 2-bit
      ..
   15     |-----| 2-bit
*/

/*
    2 bit predictor state parameter definition
*/

localparam STATE_STRONG_TAKEN      = 2'b00;
localparam STATE_WEAK_TAKEN        = 2'b01;
localparam STATE_WEAK_NOT_TAKEN    = 2'b10;
localparam STATE_STRONG_NOT_TAKEN  = 2'b11;

reg  [1:0]  HPC_branch_history_table[0:15]; //Branch History Table
wire [1:0]  branch_history_on_PC_w;
reg  [3:0]  req_branch_history_index;
wire        branch_pred_out;
reg         req_inst_branch_r;
wire        branch_pred_result;


// set initial state of BHT entries as 'STATE_WEAK_NOT_TAKEN'


//==================================
// lines below will not be provided
//==================================

always @(posedge rst_i or posedge clk) begin
    if(rst_i)                  req_branch_history_index <= 4'b0;
    else if(req_inst_branch)   req_branch_history_index <= req_branch_inst_pc[`FETCH_PC_BHT_INDEX];
end
assign branch_history_on_PC_w = HPC_branch_history_table[req_branch_inst_pc[`FETCH_PC_BHT_INDEX]];




always @(posedge rst_i or posedge clk) begin
    if (rst_i) begin
        HPC_branch_history_table[0]  <= 2'b10;
        HPC_branch_history_table[1]  <= 2'b10;
        HPC_branch_history_table[2]  <= 2'b10;
        HPC_branch_history_table[3]  <= 2'b10;
        HPC_branch_history_table[4]  <= 2'b10;
        HPC_branch_history_table[5]  <= 2'b10;
        HPC_branch_history_table[6]  <= 2'b10;
        HPC_branch_history_table[7]  <= 2'b10;
        HPC_branch_history_table[8]  <= 2'b10;
        HPC_branch_history_table[9]  <= 2'b10;
        HPC_branch_history_table[10] <= 2'b10;
        HPC_branch_history_table[11] <= 2'b10;
        HPC_branch_history_table[12] <= 2'b10;
        HPC_branch_history_table[13] <= 2'b10;
        HPC_branch_history_table[14] <= 2'b10;
        HPC_branch_history_table[15] <= 2'b10;
    end
end

always @(posedge rst_i or posedge clk) begin
    if (req_conditional_branch_r) begin
        if ((branch_history_on_PC_w == STATE_STRONG_TAKEN) 
        && (req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_STRONG_TAKEN;
        else if( (branch_history_on_PC_w == STATE_STRONG_TAKEN) 
        && (!req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_WEAK_TAKEN;
        
        else if ((branch_history_on_PC_w == STATE_WEAK_TAKEN) 
        && (req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_STRONG_TAKEN;
        else if ((branch_history_on_PC_w == STATE_WEAK_TAKEN) 
        && (!req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_WEAK_NOT_TAKEN;
        
        else if ((branch_history_on_PC_w == STATE_WEAK_NOT_TAKEN) 
        && (req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_STRONG_NOT_TAKEN;
        else if ((branch_history_on_PC_w == STATE_WEAK_NOT_TAKEN) 
        && (!req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_STRONG_TAKEN;

        else if ((branch_history_on_PC_w == STATE_STRONG_NOT_TAKEN) 
        && (req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_STRONG_NOT_TAKEN;
        else if ((branch_history_on_PC_w == STATE_STRONG_NOT_TAKEN) 
        && (!req_branch_taken_r))                            HPC_branch_history_table[req_branch_history_index] <= STATE_WEAK_NOT_TAKEN;
        end         
end

assign branch_pred_out = req_conditional_branch && (!HPC_branch_history_table[req_branch_inst_pc[`FETCH_PC_BHT_INDEX]][1]);
assign branch_pred_result = req_conditional_branch && (branch_pred_out == req_branch_taken);

    


endmodule