// int loop_test()
// {
//     int result = 0;
// 	for (int i = 0 ; i < 100; i++)
//         result += i;
        
//     return result;
// }


#define LOOP 10000

int loop_test()
{
    int result = 0;
    int a[LOOP];
    
	for (int i = 0 ; i < LOOP; i++)
            a[i] = i;
    
    
	for (int i = 0 ; i < LOOP; i++)
        result = result + a[i];

    return result;
}
